/**
 *  Copyright (c) 2017 Leonardo A. Bautista-Gomez
 *  All rights reserved
 *
 *  FTI - A multi-level checkpointing library for C/C++/Fortran applications
 *
 *  Revision 1.0 : Fault Tolerance Interface (FTI)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *  may be used to endorse or promote products derived from this software without
 *  specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  @file   tools.c
 *  @date   October, 2017
 *  @brief  Utility functions for the FTI library.
 */

#include <dirent.h>
#include <execinfo.h>
#include <stdarg.h>

#include "tools.h"

int FTI_filemetastructsize;         /**< size of FTIFF_db struct in file    */
int FTI_dbstructsize;               /**< size of FTIFF_db struct in file    */
int FTI_dbvarstructsize;            /**< size of FTIFF_db struct in file    */

#ifdef ENABLE_HDF5
int FTI_DebugCheckOpenObjects(hid_t fid, int rank) {
    ssize_t cnt;
    int howmany;
    int i;
    H5I_type_t ot;
    hid_t anobj;
    hid_t *objs;
    char name[1024];

    cnt = H5Fget_obj_count(fid, H5F_OBJ_ALL);

    if (cnt <= 0) return cnt;

    DBG_MSG("%ld object(s) open", rank, cnt);

    objs = malloc(cnt * sizeof(hid_t));

    howmany = H5Fget_obj_ids(fid, H5F_OBJ_ALL, cnt, objs);

    DBG_MSG("open objects:", rank);

    for (i = 0; i < howmany; i++) {
        anobj = *objs++;
        ot = H5Iget_type(anobj);
        H5Iget_name(anobj, name, 1024);
        DBG_MSG(" %d: type %d, name %s", rank, i, ot, name);
    }

    return howmany;
}
#endif

/*-------------------------------------------------------------------------*/
/**
  @brief      Init of the static variables
  @return     integer         FTI_SCES if successful.

  This function initializes all static variables to zero.

 **/
/*-------------------------------------------------------------------------*/
int FTI_InitExecVars(FTIT_configuration* FTI_Conf, FTIT_execution* FTI_Exec,
        FTIT_topology* FTI_Topo, FTIT_checkpoint* FTI_Ckpt,
        FTIT_injection* FTI_Inje) {
    // datablock size in file
    FTI_filemetastructsize
        = MD5_DIGEST_STRING_LENGTH
        + MD5_DIGEST_LENGTH
        + 7*sizeof(int32_t)
        + sizeof(int);

    // TODO(leobago) RS L3 only works for even file sizes.
    // This accounts for many but clearly not all cases.
    // This is to fix.
    FTI_filemetastructsize += 2 - FTI_filemetastructsize%2;

    FTI_dbstructsize
        = sizeof(int)               /* numvars */
        + sizeof(int32_t);             /* dbsize */

    FTI_dbvarstructsize
        = 2*sizeof(int)               /* numvars */
        + 2*sizeof(bool)
        + 2*sizeof(uintptr_t)
        + 2*sizeof(int32_t)
        + MD5_DIGEST_LENGTH;

    //
    //  init meta data variables
    //

    FTIT_execution      initExec = {0};
    FTIT_configuration  initConf = {0};
    FTIT_topology       initTopo = {0};
    FTIT_injection      initInje = {0};
    FTIT_checkpoint     initCkpt = {0};

    *FTI_Exec = initExec;
    *FTI_Conf = initConf;
    *FTI_Topo = initTopo;
    *FTI_Inje = initInje;

    int i = 0; for (; i < 5; i++) FTI_Ckpt[i] = initCkpt;

    return FTI_SCES;
}


/*-------------------------------------------------------------------------*/
/**
  @brief      It calculates checksum of the checkpoint file.
  @param      FTI_Exec        Execution metadata.
  @param      FTI_Data        Dataset metadata.
  @param      checksum        Checksum that is calculated.
  @return     integer         FTI_SCES if successful.

  This function calculates checksum of the checkpoint file based on
  MD5 algorithm and saves it in checksum.

 **/
/*-------------------------------------------------------------------------*/
int FTI_Checksum(FTIT_execution* FTI_Exec, FTIT_keymap* FTI_Data,
        FTIT_configuration* FTI_Conf, char* checksum) {
    int i;
    int ii = 0;

    for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
        int rest = MD5_DIGEST_STRING_LENGTH-ii;
        snprintf(&checksum[ii], rest, "%02x", FTI_Exec->integrity[i]);
        ii += 2;
    }
    return FTI_SCES;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      It compares checksum of the checkpoint file.
  @param      fileName        Filename of the checkpoint.
  @param      checksumToCmp   Checksum to compare.
  @return     integer         FTI_SCES if successful.

  This function calculates checksum of the checkpoint file based on
  MD5 algorithm. It compares calculated hash value with the one saved
  in the file.

 **/
/*-------------------------------------------------------------------------*/
int FTI_VerifyChecksum(char* fileName, char* checksumToCmp) {
    FILE *fd = fopen(fileName, "rb");
    if (fd == NULL) {
        char str[FTI_BUFS];
        snprintf(str, sizeof(str),
         "FTI failed to open file %s to calculate checksum.", fileName);
        FTI_Print(str, FTI_WARN);
        return FTI_NSCS;
    }

    MD5_CTX mdContext;
    MD5_Init(&mdContext);

    int bytes;
    unsigned char data[CHUNK_SIZE];
    while ((bytes = fread(data, 1, CHUNK_SIZE, fd)) != 0) {
        MD5_Update(&mdContext, data, bytes);
    }
    unsigned char hash[MD5_DIGEST_LENGTH];
    MD5_Final(hash, &mdContext);

    int i;
    char checksum[MD5_DIGEST_STRING_LENGTH];  // calculated checksum
    int ii = 0;
    for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
        int rest = MD5_DIGEST_STRING_LENGTH-ii;
        snprintf(&checksum[ii], rest, "%02x", hash[i]);
        ii += 2;
    }

    if (strcmp(checksum, checksumToCmp) != 0) {
        char str[FTI_BUFS];
        snprintf(str, sizeof(str),
         "TOOLS: Checksum do not match. \"%s\" file is corrupted. %s != %s",
          fileName, checksum, checksumToCmp);
        FTI_Print(str, FTI_WARN);

        fclose(fd);

        return FTI_NSCS;
    }

    fclose(fd);

    return FTI_SCES;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      It receives the return code of a function and prints a message.
  @param      result          Result to check.
  @param      message         Message to print.
  @return     integer         The same result as passed in parameter.

  This function checks the result from a function and then decides to
  print the message either as a debug message or as a warning.

 **/
/*-------------------------------------------------------------------------*/
int FTI_Try(int result, char* message) {
    char str[FTI_BUFS];
    if (result == FTI_SCES || result == FTI_DONE) {
        snprintf(str, sizeof(str), "FTI succeeded to %s", message);
        FTI_Print(str, FTI_DBUG);
    } else {
        snprintf(str, sizeof(str), "FTI failed to %s", message);
        FTI_Print(str, FTI_WARN);
        snprintf(str, sizeof(str), "Error => %s", strerror(errno));
        FTI_Print(str, FTI_WARN);
    }
    return result;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      It mallocs memory for the metadata.
  @param      FTI_Exec        Execution metadata.
  @param      FTI_Topo        Topology metadata.

  This function mallocs the memory used for the metadata storage.

 **/
/*-------------------------------------------------------------------------*/
int FTI_InitGroupsAndTypes(FTIT_execution* FTI_Exec) {
    // Allocate all data structures
    memset(&FTI_Exec->datatypes, 0, sizeof(FTIT_DataTypes));
    TRY_ALLOC(FTI_Exec->datatypes.types, FTIT_Datatype, TYPES_MAX) {
      return FTI_NSCS;
    }
    // Allocate all groups
    TRY_ALLOC(FTI_Exec->H5groups, FTIT_H5Group*, FTI_BUFS) {
      return FTI_NSCS;
    }
    // Allocate the first group
    TRY_ALLOC(FTI_Exec->H5groups[0], FTIT_H5Group, 1) {
      return FTI_NSCS;
    }
    // Initialize the first group
    FTI_Exec->nbGroup = 1;
    snprintf(FTI_Exec->H5groups[0]->name, FTI_BUFS, "/");

    // Initialize the C native datatypes
    FTI_CHAR = FTI_InitType_opaque(sizeof(char));
    FTI_SHRT = FTI_InitType_opaque(sizeof(short));
    FTI_INTG = FTI_InitType_opaque(sizeof(int));
    FTI_LONG = FTI_InitType_opaque(sizeof(long));
    FTI_UCHR = FTI_InitType_opaque(sizeof(unsigned char));
    FTI_USHT = FTI_InitType_opaque(sizeof(unsigned short));
    FTI_UINT = FTI_InitType_opaque(sizeof(unsigned int));
    FTI_ULNG = FTI_InitType_opaque(sizeof(unsigned long));
    FTI_SFLT = FTI_InitType_opaque(sizeof(float));
    FTI_DBLE = FTI_InitType_opaque(sizeof(double));
    FTI_LDBE = FTI_InitType_opaque(sizeof(long double));

#ifdef ENABLE_HDF5
    FTI_GetType(FTI_CHAR)->h5datatype = H5T_NATIVE_CHAR;
    FTI_GetType(FTI_SHRT)->h5datatype = H5T_NATIVE_SHORT;
    FTI_GetType(FTI_INTG)->h5datatype = H5T_NATIVE_INT;
    FTI_GetType(FTI_LONG)->h5datatype = H5T_NATIVE_LONG;
    FTI_GetType(FTI_UCHR)->h5datatype = H5T_NATIVE_UCHAR;
    FTI_GetType(FTI_USHT)->h5datatype = H5T_NATIVE_USHORT;
    FTI_GetType(FTI_UINT)->h5datatype = H5T_NATIVE_UINT;
    FTI_GetType(FTI_ULNG)->h5datatype = H5T_NATIVE_ULONG;
    FTI_GetType(FTI_SFLT)->h5datatype = H5T_NATIVE_FLOAT;
    FTI_GetType(FTI_DBLE)->h5datatype = H5T_NATIVE_DOUBLE;
    FTI_GetType(FTI_LDBE)->h5datatype = H5T_NATIVE_LDOUBLE;
#endif
    FTI_Exec->datatypes.nprimitives = FTI_Exec->datatypes.ntypes;
    return FTI_SCES;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      It frees memory for the types.
  @param      FTI_Exec        Execution metadata.

  This function frees the memory used for the type storage.

 **/
/*-------------------------------------------------------------------------*/
void FTI_FreeTypesAndGroups(FTIT_execution* FTI_Exec) {
    int i;
    // Free all complex structures
    for (i = 0; i < FTI_Exec->datatypes.ntypes; i++)
        if (FTI_Exec->datatypes.types[i].structure != NULL)
            free(FTI_Exec->datatypes.types[i].structure);

    // Free all type structures
    free(FTI_Exec->datatypes.types);
    memset(&FTI_Exec->datatypes, 0, sizeof(FTIT_DataTypes));

    // Free all group structures
    for (i = 0; i < FTI_Exec->nbGroup; i++)
        free(FTI_Exec->H5groups[i]);
    free(FTI_Exec->H5groups);
}

/*-------------------------------------------------------------------------*/
/**
  @brief      It erases a directory and all its files.
  @param      path            Path to the directory we want to erase.
  @param      flag            Set to 1 to activate.
  @return     integer         FTI_SCES if successful.

  This function erases a directory and all its files. It focusses on the
  checkpoint directories created by FTI so it does NOT handle recursive
  erasing if the given directory includes other directories.

 **/
/*-------------------------------------------------------------------------*/
int FTI_RmDir(char path[FTI_BUFS], int flag) {
    if (flag) {
        char str[FTI_BUFS];
        snprintf(str, sizeof(str),
         "Removing directory %s and its files.", path);
        FTI_Print(str, FTI_DBUG);

        DIR* dp = opendir(path);
        if (dp != NULL) {
            struct dirent* ep = NULL;
            while ((ep = readdir(dp)) != NULL) {
                char fil[FTI_BUFS];
                snprintf(fil, sizeof(fil), "%s", ep->d_name);
                FTI_Print(fil, FTI_DBUG);
                if ((strcmp(fil, ".") != 0) && (strcmp(fil, "..") != 0)) {
                    char fn[FTI_BUFS];
                    snprintf(fn, FTI_BUFS, "%s/%s", path, fil);
                    struct stat statbuf;
                    stat(fn, &statbuf);
                    if (S_ISDIR(statbuf.st_mode)) {
                        FTI_RmDir(fn, 1);
                    } else {
                        snprintf(str, FTI_BUFS, "File %s will be removed.", fn);
                        FTI_Print(str, FTI_DBUG);
                        if (remove(fn) == -1) {
                            if (errno != ENOENT) {
                                snprintf(str, FTI_BUFS,
                                 "Error removing target file (%s).", fn);
                                FTI_Print(str, FTI_EROR);
                            }
                        }
                    }
                }
            }
        } else {
            if (errno != ENOENT) {
                FTI_Print("Error with opendir.", FTI_EROR);
            }
        }
        if (dp != NULL) {
            closedir(dp);
        }

        if (remove(path) == -1) {
            if (errno != ENOENT) {
                FTI_Print("Error removing target directory.", FTI_EROR);
            }
        }
    }
    return FTI_SCES;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      It erases the previous checkpoints and their metadata.
  @param      FTI_Conf        Configuration metadata.
  @param      FTI_Topo        Topology metadata.
  @param      FTI_Ckpt        Checkpoint metadata.
  @param      level           Level of cleaning.
  @return     integer         FTI_SCES if successful.

  This function erases previous checkpoint depending on the level of the
  current checkpoint. Level 5 means complete clean up. Level 6 means clean
  up local nodes but keep last checkpoint data and metadata in the PFS.

 **/
/*-------------------------------------------------------------------------*/
int FTI_Clean(FTIT_configuration* FTI_Conf, FTIT_topology* FTI_Topo,
        FTIT_checkpoint* FTI_Ckpt, FTIT_execution* FTI_Exec, int level) {
    // only one process in the node has set it to 1
    int nodeFlag;
    // only one process in the FTI_COMM_WORLD has set it to 1
    int globalFlag = !FTI_Topo->splitRank;

    nodeFlag = (((!FTI_Topo->amIaHead) &&
     ((FTI_Topo->nodeRank - FTI_Topo->nbHeads) == 0)) ||
      (FTI_Topo->amIaHead)) ? 1 : 0;

    bool notDcpFtiff = !(FTI_Ckpt[4].isDcp && FTI_Conf->dcpFtiff);
    bool notDcp = !FTI_Ckpt[4].isDcp;

    if (level == 0) {
        FTI_RmDir(FTI_Conf->mTmpDir, globalFlag && notDcpFtiff);
        FTI_RmDir(FTI_Ckpt[FTI_Exec->ckptMeta.level].tmpdir, globalFlag && notDcp);
    }

    // Clean last checkpoint level 1
    if (level >= 1) {
        FTI_RmDir(FTI_Ckpt[1].metaDir, globalFlag && notDcpFtiff);
        FTI_RmDir(FTI_Ckpt[1].dir, nodeFlag && notDcp);
    }

    // Clean last checkpoint level 2
    if (level >= 2) {
        FTI_RmDir(FTI_Ckpt[2].metaDir, globalFlag && notDcpFtiff);
        FTI_RmDir(FTI_Ckpt[2].dir, nodeFlag && notDcp);
    }

    // Clean last checkpoint level 3
    if (level >= 3) {
        FTI_RmDir(FTI_Ckpt[3].metaDir, globalFlag && notDcpFtiff);
        FTI_RmDir(FTI_Ckpt[3].dir, nodeFlag && notDcp);
    }

    // Clean last checkpoint level 4
    if (level == 4 || level == 5) {
        FTI_RmDir(FTI_Ckpt[4].metaDir, globalFlag && notDcpFtiff);
        FTI_RmDir(FTI_Ckpt[4].dir, globalFlag && notDcp);
        FTI_RmDir(FTI_Ckpt[4].L4Replica, nodeFlag);
        FTI_RmDir(FTI_Ckpt[4].L4Replica, nodeFlag);
        rmdir(FTI_Ckpt[4].tmpdir);
    }
    if ((FTI_Conf->dcpPosix || FTI_Conf->dcpFtiff) && level == 5) {
        FTI_RmDir(FTI_Ckpt[4].dcpDir, !FTI_Topo->splitRank);
    }

    // If it is the very last cleaning and we DO NOT keep the last checkpoint
    if (level == 5) {
        int i=1; for(;i<4; i++) {
          rmdir(FTI_Ckpt[i].tmpdir);
        }
        int cnt=0;
        while( FTI_Conf->local_dev[cnt] ) {
          rmdir(FTI_Conf->local_dev[cnt]);
          cnt++;
        }
        cnt = 0;
        while( FTI_Conf->global_dev[cnt] ) {
          rmdir(FTI_Conf->global_dev[cnt]);
          cnt++;
        }
        char buf[FTI_BUFS];
        snprintf(buf, FTI_BUFS, "%s/Topology.fti", FTI_Conf->metadDir);
        if (remove(buf) == -1) {
            if (errno != ENOENT) {
                FTI_Print("Cannot remove Topology.fti", FTI_EROR);
            }
        }
        snprintf(buf, FTI_BUFS, "%s/Checkpoint.fti", FTI_Conf->metadDir);
        if (remove(buf) == -1) {
            if (errno != ENOENT) {
                FTI_Print("Cannot remove Checkpoint.fti", FTI_EROR);
            }
        }
        rmdir(FTI_Conf->metadDir);
    }

    // If it is the very last cleaning and we DO keep the last checkpoint
    if (level == 6) {
        int i=1; for(;i<4; i++) {
          rmdir(FTI_Ckpt[i].tmpdir);
        }
        int cnt=0;
        while( FTI_Conf->local_dev[cnt] ) {
          rmdir(FTI_Conf->local_dev[cnt]);
          cnt++;
        }
    }

    return FTI_SCES;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      generates hex string representation of hash digest.
  @param      char*             hash digest 
  @param      int               digest width 
  @return     char*             hex string of hash
 **/
/*-------------------------------------------------------------------------*/
char* FTI_GetHashHexStr(unsigned char* hash, int digestWidth,
 char* hashHexStr) {
    static char hashHexStatic[MD5_DIGEST_STRING_LENGTH];
    if (hashHexStr == NULL) {
        hashHexStr = hashHexStatic;
    }

    int i;
    int rest = MD5_DIGEST_STRING_LENGTH;
    for (i = 0; i < digestWidth; i++) {
        rest -= 2;
        snprintf(&hashHexStr[2*i], rest, "%02x", hash[i]);
    }

    return hashHexStr;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      Copy src into dest if src is not null, copy fmt otherwise
  @param      dest        Destination string buffer
  @param      src         Source string buffer
  @param      fmt         Default alternative format string

**/
/*-------------------------------------------------------------------------*/
void FTI_CopyStringOrDefault(char* dest, const char* src, char* fmt, ...) {
    if (src && strlen(src)) {
        // If src points to a non-zero string
        strncpy(dest, src, strlen(src));
    } else {
        // Else, use default format instead
        va_list args;
        va_start(args, fmt);
        vsnprintf(dest, FTI_BUFS, fmt, args);
        va_end(args);
    }
}

/*-------------------------------------------------------------------------*/
/**
  @brief      Checks if an allocated FTIT_Datatype is composite
  @param      t              A pointer to the FTIT_Datatype
  @return     int            Non-zero if true, zero if false

  A composite type contains a non-empty structure field.

**/
/*-------------------------------------------------------------------------*/
inline int FTI_IsTypeComposite(FTIT_Datatype *t) {
  return t && t->structure;
}

/*-------------------------------------------------------------------------*/
/**
  @brief      Obtains the FTIT_Datatype associated to a given type handle
  @param      handle         The data type handle
  @return     FTIT_Datatype      An external handle to represent the new type

  Returns NULL if the handle is not associated to an initialized composite type

**/
/*-------------------------------------------------------------------------*/
inline FTIT_Datatype* FTI_GetCompositeType(fti_id_t handle) {
    FTIT_Datatype* t = FTI_GetType(handle);
    if (!FTI_IsTypeComposite(t))
        return NULL;
    return t;
}

int FTI_FileCopy(const char* from, const char *to, size_t buffersize, size_t* offset, size_t count, bool overwrite) {
  
  static size_t _offset[2] = {0,0};
  
  char errstr[FTI_BUFS];

  if( access( from, R_OK ) < 0 ) {
    snprintf(errstr, FTI_BUFS, "file '%s' is not readable", from);
    FTI_Print(errstr, FTI_EROR);
    return FTI_NSCS;
  }
  
  if( (access( to, F_OK ) == 0) ) {
    if( overwrite ) {
      if( (access( to, W_OK ) < 0) ) {
        snprintf(errstr, FTI_BUFS, "file '%s' is not writable", to);
        FTI_Print(errstr, FTI_EROR);
        return FTI_NSCS;
      }
    } else {
      snprintf(errstr, FTI_BUFS, "file '%s' already exists", to);
      FTI_Print(errstr, FTI_WARN);
      return FTI_NSCS;
    }
  }
  
  if( offset == NULL ) offset = _offset;
  if( count == -1 ) {
    struct stat st;
    if( stat(from, &st) < 0 ) {
      snprintf(errstr, FTI_BUFS, "failed to stat file '%s'", from);
      FTI_Print(errstr, FTI_EROR);
      return FTI_NSCS;
    }
    count = st.st_size;
  }
  
  FILE* fr = fopen( from, "r" );
  if( !fr ) {
    snprintf(errstr, FTI_BUFS, "failed to open file '%s' for reading", from);
    FTI_Print(errstr, FTI_EROR);
    return FTI_NSCS;
  }

  if( fseek( fr, offset[0], SEEK_SET ) != 0 ) {
    snprintf(errstr, FTI_BUFS, "failed to set starting offset in file '%s'", from);
    FTI_Print(errstr, FTI_EROR);
    fclose(fr);
    return FTI_NSCS;
  }
  
  FILE* fw = fopen( to, "w" );
  if( !fw ) {
    snprintf(errstr, FTI_BUFS, "failed to open file '%s' for writing", to);
    FTI_Print(errstr, FTI_EROR);
    return FTI_NSCS;
  }
  
  if( fseek( fw, offset[1], SEEK_SET ) != 0 ) {
    snprintf(errstr, FTI_BUFS, "failed to set starting offset in file '%s'", to);
    FTI_Print(errstr, FTI_EROR);
    fclose(fr);
    fclose(fw);
    if( remove(to) < 0 ) {
      snprintf(errstr, FTI_BUFS, "failed to remove '%s' upon copy error", to);
      FTI_Print(errstr, FTI_EROR);
    }
    return FTI_NSCS;
  }
 
  int rest = 0;
  size_t check = 0;
  size_t nchunks = count / buffersize;
  size_t lastchunk = count % buffersize;
  if( lastchunk ) rest = 1;
  void* buffer = malloc( buffersize );
  int i=0; for(; i<nchunks+rest; i++) {
    size_t size = (i==nchunks) ? lastchunk : buffersize;
    size_t bytes_read = fread( buffer, 1, size, fr );
    if( ferror(fr) ) {
      snprintf(errstr, FTI_BUFS, "failed to read from file '%s'", from);
      FTI_Print(errstr, FTI_EROR);
      free(buffer);
      fclose(fr);
      fclose(fw);
      if( remove(to) < 0 ) {
        snprintf(errstr, FTI_BUFS, "failed to remove '%s' upon copy error", to);
        FTI_Print(errstr, FTI_EROR);
      }
      return FTI_NSCS;
    }
    if( feof(fr) ) {
      snprintf(errstr, FTI_BUFS, "exceeded size of file '%s'", from);
      FTI_Print(errstr, FTI_WARN);
      free(buffer);
      fclose(fr);
      fclose(fw);
      if( remove(to) < 0 ) {
        snprintf(errstr, FTI_BUFS, "failed to remove '%s' upon copy error", to);
        FTI_Print(errstr, FTI_EROR);
      }
      return FTI_NSCS;
    }
    size_t bytes_written = fwrite( buffer, 1, size, fw ); 
    if( ferror(fr) || (bytes_written < bytes_read) ) {
      snprintf(errstr, FTI_BUFS, "failed to write '%lu' bytes to file '%s' (written: %lu)", bytes_read, to, bytes_written);
      FTI_Print(errstr, FTI_EROR);
      free(buffer);
      fclose(fr);
      fclose(fw);
      if( remove(to) < 0 ) {
        snprintf(errstr, FTI_BUFS, "failed to remove '%s' upon copy error", to);
        FTI_Print(errstr, FTI_EROR);
      }
      return FTI_NSCS;
    }
    check += bytes_written;
  }

  if( check != count ) {
    snprintf(errstr, FTI_BUFS, "failed to copy the data from '%s' to '%s' (to copy: '%lu', copied: %lu)", from, to, count, check);
    FTI_Print(errstr, FTI_WARN);
    free(buffer);
    fclose(fr);
    fclose(fw);
    if( remove(to) < 0 ) {
      snprintf(errstr, FTI_BUFS, "failed to remove '%s' upon copy error", to);
      FTI_Print(errstr, FTI_EROR);
    }
    return FTI_NSCS;
  }

  free(buffer);
  fclose(fr);
  fclose(fw);

  return FTI_SCES;
  
}

int FTI_Move_( FTIT_configuration* FTI_Conf, FTIT_execution* FTI_Exec, 
    FTIT_topology* FTI_Topo, int id, int level, int deviceNew ) {
  char errstr[FTI_BUFS];
  if( (deviceNew < 0) || (deviceNew > FTI_MAX_STORAGE_DEV) ) {
    snprintf(errstr, FTI_BUFS, "Invalid device id '%d'", deviceNew);
    FTI_Print(errstr, FTI_WARN);
    return FTI_NSCS;
  }
  FTIT_stat st;
  if( FTI_Stat( id, &st ) != FTI_SCES ) {
    snprintf(errstr, FTI_BUFS, "Checkpoint with id '%d' does not exist", id);
    FTI_Print(errstr, FTI_WARN);
    return FTI_NSCS;
  }
  if( !FTI_StatCheckLevel(st, level) ) {
    snprintf(errstr, FTI_BUFS, "Checkpoint with id '%d' has no level '%d'", id, level);
    FTI_Print(errstr, FTI_WARN);
    return FTI_NSCS;
  }
  int deviceOld = FTI_StatGetDevice(st, level);
  if( deviceOld == deviceNew ) {
    snprintf(errstr, FTI_BUFS, "Checkpoint with id '%d' and level '%d' is already stored at device '%d'", id, level, deviceNew);
    FTI_Print(errstr, FTI_WARN);
    return FTI_NSCS;
  }
  if( (level == FTI_L4) && (FTI_Conf->global_dev[deviceNew] == NULL) ) {
    snprintf(errstr, FTI_BUFS, "Device id '%d' is not registered at level '%d'", deviceNew, level);
    FTI_Print(errstr, FTI_WARN);
    return FTI_NSCS;
  }
  if( (level != FTI_L4) && (FTI_Conf->local_dev[deviceNew] == NULL) ) {
    snprintf(errstr, FTI_BUFS, "Device id '%d' is not registered at level '%d'", deviceNew, level);
    FTI_Print(errstr, FTI_WARN);
    return FTI_NSCS;
  }
  // only for level 1 and level 4 currently
  if( !((level == FTI_L4) || (level == FTI_L1)) ) {
    FTI_Print("FTI_Move only supports level 1 and level 4", FTI_WARN);
    return FTI_NSCS;
  }
  
  char old[FTI_BUFS];
  char tmp[FTI_BUFS];
  char new[FTI_BUFS];
  
  bool master = ( level == FTI_L4 ) ? FTI_Topo->masterGlobal : FTI_Topo->masterLocal;
  
  // set directory names fro new device
  if( level == FTI_L4 ) {
    snprintf(old, FTI_BUFS, "%s/%s/l4/%d", FTI_Conf->global_dev[deviceOld], FTI_Exec->id, id);
    snprintf(tmp, FTI_BUFS, "%s/%s/tmp", FTI_Conf->global_dev[deviceNew], FTI_Exec->id);
    snprintf(new, FTI_BUFS, "%s/%s/l4/%d", FTI_Conf->global_dev[deviceNew], FTI_Exec->id, id);
  } else {
    if (FTI_Conf->test) {  // If local test generate name by topology
      int node = FTI_Topo->myRank / FTI_Topo->nodeSize;
      snprintf(old, FTI_BUFS, "%s/node%d/%s/l1/%d", FTI_Conf->local_dev[deviceOld], node, FTI_Exec->id, id);
      snprintf(tmp, FTI_BUFS, "%s/node%d/%s/tmp", FTI_Conf->local_dev[deviceNew], node, FTI_Exec->id);
      snprintf(new, FTI_BUFS, "%s/node%d/%s/l1/%d", FTI_Conf->local_dev[deviceNew], node, FTI_Exec->id, id);
    } else {
      snprintf(old, FTI_BUFS, "%s/%s/l1/%d", FTI_Conf->local_dev[deviceOld], FTI_Exec->id, id);
      snprintf(tmp, FTI_BUFS, "%s/%s/tmp", FTI_Conf->local_dev[deviceNew], FTI_Exec->id);
      snprintf(new, FTI_BUFS, "%s/%s/l1/%d", FTI_Conf->local_dev[deviceNew], FTI_Exec->id, id);
    }
  }
  
  if( master ) MKDIR(tmp, 0777);
  
  int* ranks;
  int startProc = 0, endProc;
  if (FTI_Topo->amIaHead) {
    endProc = FTI_Topo->nodeSize-1;
    ranks = FTI_Topo->body;
  } else {
    endProc = 1;
    ranks = &FTI_Topo->myRank;
  }
 
  // need to wait for directory creation
  MPI_Barrier( FTI_COMM_WORLD );
  
  int res = FTI_SCES, allRes;

  int proc; for (proc = startProc; proc < endProc; proc++) {
    char file[FTI_BUFS], from[FTI_BUFS], to[FTI_BUFS];
    snprintf(file, FTI_BUFS, "Ckpt%d-Rank%d.%s", id, ranks[proc], FTI_Conf->suffix);
    snprintf(errstr, FTI_BUFS, "copy checkpoint file '%s'", file);
    snprintf(from, FTI_BUFS, "%s/%s", old, file);
    snprintf(to, FTI_BUFS, "%s/%s", tmp, file);
    res = FTI_Try( FTI_FileCopy( from, to, FTI_COPY_STD_BSIZE, NULL, -1, false ), errstr);
  }
  
  // need to wait for completion
  MPI_Allreduce(&res, &allRes, 1, MPI_INT, MPI_SUM, FTI_COMM_WORLD);
  if( allRes != FTI_SCES ) {
    FTI_RmDir( tmp, master );
    return FTI_NSCS;
  }
 
  res = FTI_SCES;

  if( master ) {
    res = rename( tmp, new );
    if( res != 0 ) {
      snprintf(errstr, FTI_BUFS, "failed to rename %s to %s", tmp, new);
      FTI_Print(errstr, FTI_EROR);
    }
  }
    
  // need to wait for completion
  MPI_Allreduce(&res, &allRes, 1, MPI_INT, MPI_SUM, FTI_COMM_WORLD);
  if( allRes != FTI_SCES ) {
    FTI_RmDir( tmp, master );
    return FTI_NSCS;
  }

  res = FTI_SCES;
  
  if( FTI_Topo->masterGlobal ) {
    char fn[FTI_BUFS];
    snprintf(fn, FTI_BUFS, "%s/Checkpoint.fti", FTI_Conf->metadDir);
    FTIT_iniparser ini;
    res = FTI_Iniparser(&ini, fn, FTI_INI_APPEND);
    if (res == FTI_SCES) {
      char section[FTI_BUFS], key[FTI_BUFS], value[FTI_BUFS];
      snprintf(section, FTI_BUFS, "checkpoint_id.%d", id);
      snprintf(key, FTI_BUFS, "%s:[l%d]device", section, level);
      snprintf(value, FTI_BUFS, "%d", deviceNew);
      ini.set(&ini, key, value);
      res = ini.dump(&ini);
      if (res == FTI_SCES) {
        ini.clear(&ini);
      } else {
        FTI_Print("unable to store checkpoint meta data info", FTI_WARN);
      }
    } else {
      FTI_Print("FTI failed to load the checkpoint meta data", FTI_WARN);
    }
  }
  
  MPI_Allreduce(&res, &allRes, 1, MPI_INT, MPI_SUM, FTI_COMM_WORLD);
  if( allRes != FTI_SCES ) {
    if(master) rename( tmp, new );
    return FTI_NSCS;
  } else {
    FTI_RmDir( old, master );
  }
  
  return FTI_SCES;

}

