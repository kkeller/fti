/**
 *  Copyright (c) 2017 Leonardo A. Bautista-Gomez
 *  All rights reserved
 *
 *  FTI - A multi-level checkpointing library for C/C++/Fortran applications
 *
 *  Revision 1.0 : Fault Tolerance Interface (FTI)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *  may be used to endorse or promote products derived from this software without
 *  specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  @file   meta.c
 *  @date   October, 2017
 *  @brief  Metadata functions for the FTI library.
 */

#include "stat.h"

bool FTI_StatId( FTIT_iniparser* ini, int id ) {
  char section[FTI_BUFS];
  snprintf(section, FTI_BUFS, "checkpoint_id.%d", id);
  return ini->isSection(ini, section);
}

int FTI_StatPopulateLevel( FTIT_iniparser* ini, int id, FTIT_stat* st ) {
  
  char section[FTI_BUFS];
  snprintf(section, FTI_BUFS, "checkpoint_id.%d", id);
  
  char key[FTI_BUFS];
  snprintf(key, FTI_BUFS, "%s:levels", section);
  char* ckptLvelRaw = ini->getString(ini, key);

  if (strlen(ckptLvelRaw) == 0) {
      char dbg[FTI_BUFS];
      snprintf( dbg, FTI_BUFS, "FTI_StatPopulateLevel, ckpt-id: %d -> Cannot find checkpoint id!", id );
      FTI_Print( dbg, FTI_DBUG );
      return FTI_NSCS;
  }

  char delim[2] = ",";
  char* token = strtok( ckptLvelRaw, delim );
  while( token ) {
    switch( atoi(token) ) {
      case 1:
        st->level |= FTI_ST_LEVEL_1;
        break;
      case 2:
        st->level |= FTI_ST_LEVEL_2;
        break;
      case 3:
        st->level |= FTI_ST_LEVEL_3;
        break;
      case 4:
        st->level |= FTI_ST_LEVEL_4;
        break;
      default:
        FTI_Print("Unknown Level!", FTI_WARN);
        return FTI_NSCS;
    }
    token = strtok( NULL, delim );
  }
  
  return FTI_SCES;

}

int FTI_StatPopulateDcp( FTIT_iniparser* ini, int id, FTIT_stat* st ) {

  char section[FTI_BUFS];
  snprintf(section, FTI_BUFS, "checkpoint_id.%d", id);
  
  char key[FTI_BUFS];
  if( FTI_ST_IS_LEVEL_1(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l1]is_dcp", section);
    if( ini->getBool(ini, key) ) st->dcp |= FTI_ST_DCP_LEVEL_1;
  }
  if( FTI_ST_IS_LEVEL_2(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l2]is_dcp", section);
    if( ini->getBool(ini, key) ) st->dcp |= FTI_ST_DCP_LEVEL_2;
  }
  if( FTI_ST_IS_LEVEL_3(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l3]is_dcp", section);
    if( ini->getBool(ini, key) ) st->dcp |= FTI_ST_DCP_LEVEL_3;
  }
  if( FTI_ST_IS_LEVEL_4(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l4]is_dcp", section);
    if( ini->getBool(ini, key) ) st->dcp |= FTI_ST_DCP_LEVEL_4;
  }

  return FTI_SCES;
  
}

int FTI_StatPopulateDevice( FTIT_iniparser* ini, int id, FTIT_stat* st ) {

  char section[FTI_BUFS];
  snprintf(section, FTI_BUFS, "checkpoint_id.%d", id);
  
  uint16_t one = 0x1;
  char key[FTI_BUFS];
  if( FTI_ST_IS_LEVEL_1(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l1]device", section);
    int device = ini->getInt(ini, key);
    if( (device >= 0) && (device < FTI_MAX_STORAGE_DEV) ) {
      st->device |= (one << device); 
    } else {
      FTI_Print("Invalid value for device", FTI_WARN);
      return FTI_NSCS;
    }
  }
  if( FTI_ST_IS_LEVEL_2(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l2]device", section);
    int device = ini->getInt(ini, key);
    if( (device >= 0) && (device < FTI_MAX_STORAGE_DEV) ) {
      st->device |= (one << (4+device)); 
    } else {
      FTI_Print("Invalid value for device", FTI_WARN);
      return FTI_NSCS;
    }
  }
  if( FTI_ST_IS_LEVEL_3(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l3]device", section);
    int device = ini->getInt(ini, key);
    if( (device >= 0) && (device < FTI_MAX_STORAGE_DEV) ) {
      st->device |= (one << (8+device)); 
    } else {
      FTI_Print("Invalid value for device", FTI_WARN);
      return FTI_NSCS;
    }
  }
  if( FTI_ST_IS_LEVEL_4(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l4]device", section);
    int device = ini->getInt(ini, key);
    if( (device >= 0) && (device < FTI_MAX_STORAGE_DEV) ) {
      st->device |= (one << (12+device)); 
    } else {
      FTI_Print("Invalid value for device", FTI_WARN);
      return FTI_NSCS;
    }
  }

  return FTI_SCES;
  
}

int FTI_StatPopulateElastic( FTIT_iniparser* ini, int id, FTIT_stat* st ){
  
  char section[FTI_BUFS];
  snprintf(section, FTI_BUFS, "checkpoint_id.%d", id);
  
  char key[FTI_BUFS];
  if( FTI_ST_IS_LEVEL_1(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l1]is_elastic", section);
    if( ini->getBool(ini, key) ) st->elastic |= FTI_ST_ELASTIC_LEVEL_1;
  }
  if( FTI_ST_IS_LEVEL_2(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l2]is_elastic", section);
    if( ini->getBool(ini, key) ) st->elastic |= FTI_ST_ELASTIC_LEVEL_2;
  }
  if( FTI_ST_IS_LEVEL_3(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l3]is_elastic", section);
    if( ini->getBool(ini, key) ) st->elastic |= FTI_ST_ELASTIC_LEVEL_3;
  }
  if( FTI_ST_IS_LEVEL_4(st->level) ) {
    snprintf(key, FTI_BUFS, "%s:[l4]is_elastic", section);
    if( ini->getBool(ini, key) ) st->elastic |= FTI_ST_ELASTIC_LEVEL_4;
  }
  
  return FTI_SCES;
  
}
 
int FTI_StatGetDevice( FTIT_stat st, int level ) {
  switch( level ) {
    case FTI_L1:
      if( !FTI_ST_IS_LEVEL_1(st.level) ) {
        FTI_Print( "checkpoint does not have level 1", FTI_WARN);
        return FTI_NSCS; 
      }
      break;
    case FTI_L2:
      if( !FTI_ST_IS_LEVEL_2(st.level) ) {
        FTI_Print( "checkpoint does not have level 2", FTI_WARN);
        return FTI_NSCS; 
      }
      break;
    case FTI_L3:
      if( !FTI_ST_IS_LEVEL_3(st.level) ) {
        FTI_Print( "checkpoint does not have level 3", FTI_WARN);
        return FTI_NSCS; 
      }
      break;
    case FTI_L4:
      if( !FTI_ST_IS_LEVEL_4(st.level) ) {
        FTI_Print( "checkpoint does not have level 4", FTI_WARN);
        return FTI_NSCS; 
      }
      break;
    default:
      FTI_Print( "Invalid level", FTI_WARN);
      return FTI_NSCS; 
  }
  
  uint16_t val = (st.device >> ((level-1)*4))&(uint16_t)0xf;
  int device = -1;
  for(; val>0; val >>= 1) {
    device++;
  }

  return device;
}

bool FTI_StatCheckLevel( FTIT_stat st, int level ) {
  
  switch( level ) {
    case 1:
      if(!FTI_ST_IS_LEVEL_1( st.level ))
        return false;
      return true;
    case 2:
      if(!FTI_ST_IS_LEVEL_2( st.level ))
        return false;
      return true;
    case 3:
      if(!FTI_ST_IS_LEVEL_3( st.level ))
        return false;
      return true;
    case 4:
      if(!FTI_ST_IS_LEVEL_4( st.level ))
        return false;
      return true;
    default:
      FTI_Print("Unknown Level!", FTI_WARN);
      return false;
  }

}
