/**
 *  Copyright (c) 2017 Leonardo A. Bautista-Gomez
 *  All rights reserved
 *
 *  FTI - A multi-level checkpointing library for C/C++/Fortran applications
 *
 *  Revision 1.0 : Fault Tolerance Interface (FTI)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *  may be used to endorse or promote products derived from this software without
 *  specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  @file   checkpoint.c
 *  @date   October, 2017
 *  @brief  Checkpointing functions for the FTI library.
 */

#include "convert.h"

int FTI_ConvertActivateHeads(FTIT_configuration* FTI_Conf,
  FTIT_execution* FTI_Exec, FTIT_topology* FTI_Topo,
  FTIT_checkpoint* FTI_Ckpt, bool L4toL1, bool L1toL4) {
    // Head needs ckpt. ID to determine ckpt file name.
    int value = FTI_BASE + FTI_Exec->ckptMeta.level;  // Token to send to head
    MPI_Send(&value, 1, MPI_INT, FTI_Topo->headRank, FTI_Conf->convertTag,
     FTI_Exec->globalComm);
    MPI_Send(&FTI_Ckpt[FTI_Exec->ckptMeta.level].deviceId, 1, MPI_INT, FTI_Topo->headRank, FTI_Conf->convertTag,
     FTI_Exec->globalComm);
    MPI_Send(&FTI_Exec->ckptMeta.ckptId, 1, MPI_INT, FTI_Topo->headRank,
     FTI_Conf->convertTag, FTI_Exec->globalComm);
    MPI_Send(&L4toL1, 1, MPI_C_BOOL, FTI_Topo->headRank,
     FTI_Conf->convertTag, FTI_Exec->globalComm);
    MPI_Send(&L1toL4, 1, MPI_C_BOOL, FTI_Topo->headRank,
     FTI_Conf->convertTag, FTI_Exec->globalComm);
    return FTI_SCES;
}

int FTI_ConvertPerform( FTIT_checkpoint* FTI_Ckpt, FTIT_execution* FTI_Exec,
    FTIT_topology* FTI_Topo, FTIT_configuration* FTI_Conf ) {
  FTIT_stat st;
  if( FTI_Stat( FTI_Exec->ckptMeta.ckptId , &st ) != FTI_SCES ) {
    FTI_Print("stat error", FTI_WARN);
    return FTI_NSCS;
  }
  
  // set storage device, ckpt file and ckpt ID
  int device = FTI_StatGetDevice( st, FTI_Exec->ckptMeta.level );
  if( device == FTI_NSCS ) return FTI_NSCS;
  FTI_SetStorageDevice( FTI_Exec->ckptMeta.level, device );

  // flush
  FTI_Try(FTI_Flush(FTI_Conf, FTI_Exec, FTI_Topo,
        FTI_Ckpt, FTI_Exec->ckptMeta.level), "save the last ckpt. in the PFS.");
  MPI_Barrier(FTI_COMM_WORLD);
  if (FTI_Topo->splitRank == 0) {
    if (access(FTI_Ckpt[4].dir, 0) == 0) {
      // Delete previous L4 checkpoint
      // FTI_RmDir(FTI_Ckpt[4].dir, 1);
    }
    char l4ckptdir[FTI_BUFS];
    snprintf( l4ckptdir, FTI_BUFS, "%s/%u", FTI_Ckpt[4].dir, FTI_Exec->ckptMeta.ckptId );
    RENAME(FTI_Ckpt[4].tmpdir, l4ckptdir);
    if ( FTI_Conf->ioMode != FTI_IO_FTIFF ) {
      if (access(FTI_Ckpt[4].metaDir, 0) == 0) {
        // Delete previous L4 metadata
        //FTI_RmDir(FTI_Ckpt[4].metaDir, 1);
      }
      char l4metadir[FTI_BUFS];
      char localmetadir[FTI_BUFS];
      snprintf( l4metadir, FTI_BUFS, "%s/%u", FTI_Ckpt[4].metaDir, FTI_Exec->ckptMeta.ckptId );
      snprintf( localmetadir, FTI_BUFS, "%s/%u", FTI_Ckpt[FTI_Exec->ckptMeta.level].metaDir, FTI_Exec->ckptMeta.ckptId );
      RENAME(localmetadir,l4metadir);
    }
  }
 
  FTI_Exec->ckptMeta.level = 4;
  int globalFlag = !FTI_Topo->splitRank;
  globalFlag = (!(FTI_Ckpt[4].isDcp && FTI_Conf->dcpFtiff) &&
      (globalFlag != 0));
  if (globalFlag) {  // True only for one process in the FTI_COMM_WORLD.
    if ( FTI_Conf->ioMode != FTI_IO_FTIFF ) {
      FTI_WriteCkptMetaData(FTI_Conf, FTI_Exec, FTI_Topo, FTI_Ckpt);
    }
  }
  MPI_Barrier(FTI_COMM_WORLD);  // barrier needed to wait for process to

}
