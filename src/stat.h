/**
 *  Copyright (c) 2017 Leonardo A. Bautista-Gomez
 *  All rights reserved
 *
 *  @file   meta.h
 */

#ifndef FTI_SRC_STAT_H_
#define FTI_SRC_STAT_H_

#include "interface.h"

bool FTI_StatId( FTIT_iniparser* ini, int id );
int FTI_StatPopulateLevel( FTIT_iniparser* ini, int id, FTIT_stat* st );
int FTI_StatPopulateDevice( FTIT_iniparser* ini, int id, FTIT_stat* st );
int FTI_StatPopulateElastic( FTIT_iniparser* ini, int id, FTIT_stat* st );
int FTI_StatPopulateDcp( FTIT_iniparser* ini, int id, FTIT_stat* st );
int FTI_StatGetDevice( FTIT_stat st, int level );
bool FTI_StatCheckLevel( FTIT_stat st, int level );

#endif // FTI_SRC_STAT_H_
