/**
 *  Copyright (c) 2017 Leonardo A. Bautista-Gomez
 *  All rights reserved
 *
 *  @file   meta.h
 */

#ifndef FTI_SRC_CONVERT_H_
#define FTI_SRC_CONVERT_H_

#include "interface.h"

int FTI_ConvertActivateHeads(FTIT_configuration* FTI_Conf,
  FTIT_execution* FTI_Exec, FTIT_topology* FTI_Topo,
  FTIT_checkpoint* FTI_Ckpt, bool L4toL1, bool L1toL4);
int FTI_ConvertPerform( FTIT_checkpoint* FTI_Ckpt, FTIT_execution* FTI_Exec,
    FTIT_topology* FTI_Topo, FTIT_configuration* FTI_Conf );

#endif // FTI_SRC_CONVERT_H_
